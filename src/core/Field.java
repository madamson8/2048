package core;

import java.util.Random;

import static WindowManagers.BasicPanel.rectList;

@SuppressWarnings("Duplicates")
public class Field {

    public static int numberOfMovements = 0;

    /*
     * TODO
     * Fix a bug where on a field that looks like the following:
     * 0000
     * 0240
     * 0000
     * 0000
     * When changing direction such as left or right values skip.
     * Specifically, if right is pressed, the 4 will shift to the right but the 2 will not.
     * This results in a field that looks like follows:
     * 0000
     * 0204
     * 0000
     * 0000
     * ------------------
     * TODO
     * new numbers have the ability to generate on top of existing numbers.
     */

    public Field() {

    }

    public void shuffle(String direction) {
        /*
         * First move all tiles as far as they can go in a certain direction
         * After that calculate which tiles are the same and repeat the process.
         */
        numberOfMovements++;
        switch (direction) {
            case "left":
                for(int x = 3; x >= 0; x--) {
                    for(int y = 3; y >= 0; y--) {
                        if(y != 0) {
                            //Merge similar values
                            if(rectList[x][y].getValue() == rectList[x][y-1].getValue()) {
                                rectList[x][y-1].setValue(rectList[x][y-1].getValue()*2);
                            }
                            //Check values to left for 0s
                            if(rectList[x][y].getValue() != 0 && rectList[x][y-1].getValue() == 0) {
                                rectList[x][y-1].setValue(rectList[x][y].getValue());
                                rectList[x][y].setValue(0);
                            }
                        }
                    }
                }
                break;
            case "right":
                for(int x = 0; x < rectList[0].length; x++) {
                    for(int y = 0; y < rectList[1].length; y++) {
                        if(y != 3) {
                            if(rectList[x][y].getValue() == rectList[x][y+1].getValue()) {
                                rectList[x][y+1].setValue(rectList[x][y+1].getValue()*2);
                            }

                            if(rectList[x][y].getValue() != 0 && rectList[x][y+1].getValue() == 0) {
                                rectList[x][y+1].setValue(rectList[x][y].getValue());
                                rectList[x][y].setValue(0);
                            }
                        }
                    }
                }
                break;
            case "up":
                for(int x = 3; x >= 0; x--) {
                    for(int y = 3; y >= 0; y--) {
                        if(x != 0) {
                            //Merge similar values
                            if(rectList[x][y].getValue() == rectList[x-1][y].getValue()) {
                                rectList[x-1][y].setValue(rectList[x-1][y].getValue()*2);
                            }
                            //Check values to left for 0s
                            if(rectList[x][y].getValue() != 0 && rectList[x-1][y].getValue() == 0) {
                                rectList[x-1][y].setValue(rectList[x][y].getValue());
                                rectList[x][y].setValue(0);
                            }
                        }
                    }
                }
                break;
            case "down":
                for(int x = 0; x < rectList[0].length; x++) {
                    for(int y = 0; y < rectList[1].length; y++) {
                        if(x != 3) {
                            if(rectList[x][y].getValue() == rectList[x+1][y].getValue()) {
                                rectList[x+1][y].setValue(rectList[x+1][y].getValue()*2);
                            }

                            if(rectList[x][y].getValue() != 0 && rectList[x+1][y].getValue() == 0) {
                                rectList[x+1][y].setValue(rectList[x][y].getValue());
                                rectList[x][y].setValue(0);
                            }
                        }
                    }
                }
        }
    }

    public void loadNewNumber() {
        Random r = new Random();
        int f = r.nextInt(4);
        int s = r.nextInt(4);
        if(rectList[f][s].getValue() == 0) {
            if(r.nextInt(2) == 0) {
                rectList[f][s].setValue(2);
            } else {
                rectList[f][s].setValue(4);
            }
        }
    }

    public static int getHighScore() {
        int score = 0;
        for(int i = 0; i < rectList[0].length; i++) {
            for(int y = 0; y < rectList[1].length; y++) {
                if(rectList[i][y].getValue() > 0) {
                    score += rectList[i][y].getValue();
                }
            }
        }
        return score;
    }
}
