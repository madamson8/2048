package core;

import java.awt.*;

public class Rectangle {

    private int x, y, width, height;
    private int value;
    private Color color;

    public Rectangle(int id) {
        // If x is divisible by 1 only, x = 30. If x is divisible by 2 but not 4, x = 115, if by 3, then 190, if by 4 275
//        this.x = (85*id)+30;
        // 4 % 2 && 4 % 4 == 0
        // 2 % 2 == 0 2 % 4 == 2
        if(id == 2 || id == 6 || id == 10 || id == 14) {
            this.x = 115;
        } else if (id == 3 || id == 7 || id == 11 || id == 15) {
            this.x = 200;
        } else if (id == 4 || id == 8 || id == 12 || id == 16) {
            this.x = 285;
        } else {
            this.x = 30;
        }
        if(id-1 < 4) {
            this.y = 30;
        } else if (id-1 < 8) {
            this.y = 130;
        } else if (id-1 < 12) {
            this.y = 230;
        } else {
            this.y = 330;
        }
        this.width = 85;
        this.height = 85;
        this.color = Color.BLACK;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getValue() {
        return value;
    }

    public Color getColor() {
        return color;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
