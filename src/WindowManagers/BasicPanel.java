package WindowManagers;

import core.Field;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

@SuppressWarnings("Duplicates")
public class BasicPanel extends JPanel {

    public static core.Rectangle[][] rectList = new core.Rectangle[4][4];

    BasicPanel() {
        setPreferredSize(new Dimension(400, 400));

        int id = 1;

        for(int i = 0; i < rectList[0].length; i++) {
            for(int y = 0; y < rectList[1].length; y++) {
                rectList[i][y] = new core.Rectangle(id);
                id++;
            }
        }

        generateFieldData();

    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        for(int i = 0; i < 4; i++) {
            for(int y = 0; y < 4; y++) {
                g.setColor(rectList[y][i].getColor());

                g.drawRect(rectList[i][y].getX(), rectList[i][y].getY(), rectList[i][y].getWidth(), rectList[i][y].getHeight());
                g.drawString(String.valueOf(rectList[i][y].getValue()), rectList[i][y].getX()+40, rectList[i][y].getY()+45);
            }
        }

        g.setColor(Color.BLACK);
        g.drawRect(25, 25, 350, 450);
        g.drawString("Score: " + Field.getHighScore(), 75, 435);
        g.drawString("Movements: " + Field.numberOfMovements, 150, 435);
    }

    private void generateFieldData() {
        Random r = new Random();
        int firstSelection = r.nextInt(3);
        int secondSelection = r.nextInt(3);

        int firstSelection2 = r.nextInt(3);
        int secondSelection2 = r.nextInt(3);


        if(r.nextInt(2) == 0) {
            rectList[firstSelection][secondSelection].setValue(2);
        } else {
            rectList[firstSelection][secondSelection].setValue(4);
        }
        if(r.nextInt(2) == 0) {
            rectList[firstSelection2][secondSelection2].setValue(2);
        } else {
            rectList[firstSelection2][secondSelection2].setValue(4);
        }
    }
}
