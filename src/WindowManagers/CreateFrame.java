package WindowManagers;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import core.Field;

public class CreateFrame extends JFrame {

    public CreateFrame(String name) {
        Field f = new Field();

        this.setName(name);
        this.setTitle(name);
        this.setSize(400, 600);
        this.setResizable(false);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        this.add(new BasicPanel());

        this.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {
            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
                if(keyEvent.getKeyCode() == KeyEvent.VK_LEFT) {
//                    System.out.println("Left Key Pressed.");
                    f.shuffle("left");
                    revalidate();
                    repaint();
                } else if (keyEvent.getKeyCode() == KeyEvent.VK_UP) {
                    f.shuffle("up");
                    revalidate();
                    repaint();
                } else if (keyEvent.getKeyCode() == KeyEvent.VK_RIGHT) {
                    f.shuffle("right");
                    revalidate();
                    repaint();
                } else if (keyEvent.getKeyCode() == KeyEvent.VK_DOWN) {
                    f.shuffle("down");
                    revalidate();
                    repaint();
                }
                f.loadNewNumber();
            }
        });

        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
}
